
import React from 'react';
import './App.css';
import { 
    BrowserRouter,
    Route
} from 'react-router-dom';
import HeaderContainer from './components/Header/HeaderContainer';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import AllItems from './components/AllItems/AllItems';
import Footer from './components/Footer/Footer';
import UserAuction from './components/UserAuction/UserAuction';
import AddItem from './components/AddItem/AddItem';
import ContainerAuction from './components/ContainerAuction/ContainerAuction';
import AboutUs from './components/AboutUs/AboutUs';
import UserPurchases from './components/UserPurchases/UserPurchases';
import Favorites from './components/Favorites/Favorites';
import UserSettings from './components/UserSettings/UserSettings';
import AuctionItem from './components/AllItems/AllItems';
import EditItem from './components/EditItem/EditItem';
import ContactUs from './components/ContactUs/ContactUs';
import ProductLive from './components/ProductLive/ProductLive';
import CarouselMain from './components/Carousel/CarouselMain';


const App = () => {
    
    return (
        <BrowserRouter >
            <HeaderContainer />
            <div className = 'app-wrapper pb-5'>
                <Route exact path="/">
                    <CarouselMain />
                    <ContainerAuction />
                </Route>
                <Route path="/auction">
                    <CarouselMain />
                    <ContainerAuction />
                </Route>
                <Route path="/login">
                    <Login />
                </Route>
                <Route path="/registration">
                    <Registration />
                </Route>
                <Route path="/allItems">
                    <AllItems />
                </Route>
                <Route path="/userAuction">
                    <UserAuction />
                </Route>
                <Route path="/addItem">
                    <AddItem />
                </Route>
                <Route path="/about_us">
                    <AboutUs />
                </Route>
                <Route path="/home">
                    <ContainerAuction />
                </Route>
                <Route path="/favorites">
                    <Favorites />
                </Route>
                <Route path="/purchases">
                    <UserPurchases />
                </Route>
                <Route path="/usersettings">
                    <UserSettings />
                </Route>
                <Route path="/search">
                    <AuctionItem />
                </Route>
                <Route path="/edititem">
                    <EditItem />
                </Route>
                <Route path="/contact_us">
                    <ContactUs />
                </Route>
                <Route path="/product_live">
                    <ProductLive />
                </Route>
               
               <Footer />
            </div>    
        </BrowserRouter>
    )
}

export default App;