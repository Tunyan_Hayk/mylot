import React from "react";
import LiveAuctionContainer from "./LiveAuction/LiveAuctionContainer";
import LastChanseAuctionContainer from "./LastChanseAuction/LastChanseAuctionContainer";
import ExpectedAuctionContainer from "./ExpectedAuction/ExpectedAuctionContainer";
import Suggestitem from "../SuggestItem/SuggestItem";
import SuggestedAddItem from "../SuggestedAddItem/SuggestedAddItem";
import { Link } from "react-router-dom";

const  ContainerAuction = () => {

    return (
        <div className="container py-5">
             <div className={` row `}>
                <div className={`col-lg-9 mt-5 mt-lg-0 order-1 order-xl-0`}>
                    <div className={`card `}>
                        <h3 className="text-center header_blue mt-3">Live Auction</h3>
                        <div className="col-12">
                            <div className="row">                    
                                <div className="col-12 " >
                                    <LiveAuctionContainer />
                                </div>
                            </div>
                            <div className="d-flex justify-content-end py-2">
                                <Link to="/search">
                                   <span className="header_blue">View More >></span> 
                                </Link>
                            </div>
                        </div>
                    </div>
                    {/* Last Chanse */}
                    <div className={`card mt-5`}>
                        <h3 className="text-center header_blue mt-3">Last Chanse</h3>
                        <div className="col-12">
                            <div className="row">                            
                                <div className="col-12 " >
                                    <LastChanseAuctionContainer />
                                </div>                        
                            </div>
                            <div className="d-flex justify-content-end py-2">
                                <Link to="/search">
                                   <span className="header_blue">View More >></span> 
                                </Link>
                            </div>
                        </div>
                    </div>
                    {/* Expected Auction */}
                    <div className={`card mt-5`}>
                        <h3 className="text-center header_blue mt-3">Expected Auction</h3>
                        <div className="col-12">
                            <div className="row">
                                <div className="col-12 " >
                                    <ExpectedAuctionContainer />
                                </div>                            
                            </div>
                            <div className="d-flex justify-content-end py-2">
                                <Link to="/search">
                                   <span className="header_blue">View More >></span> 
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`col-lg-3 order-0 order-lg-1`}>
                    <div className={`card`} >
                        <Suggestitem />
                        <SuggestedAddItem />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ContainerAuction