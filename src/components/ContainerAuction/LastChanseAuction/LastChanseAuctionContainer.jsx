import React from 'react'
import { connect } from 'react-redux'
import {getLastProducts } from '../../../redux/LastChanseAuctionReducer'
import LastChanseAuction from './LastChanseAuction'

class LastChanseAuctionContainer extends React.Component{
    componentDidMount (){
        this.props.getLastProducts()
    }  
    render () {
        return <LastChanseAuction lastChanseProducts = {this.props.lastChanseProducts} /> 
    }
}

let mapStateToProps = (state) =>{
    return {
        lastChanseProducts : state.lastChanse.lastChanseProducts,
    }
}


export default connect(mapStateToProps, {
    getLastProducts
})(LastChanseAuctionContainer)