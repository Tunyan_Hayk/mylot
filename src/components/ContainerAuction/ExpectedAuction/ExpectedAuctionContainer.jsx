import React from 'react'
import { connect } from 'react-redux'
import {getUpcomingProducts } from '../../../redux/ExpectedAuctionReducer'
import ExpectedAuction from './ExpectedAuction'

class ExpectedAuctionContainer extends React.Component{
    componentDidMount (){
        this.props.getUpcomingProducts()
    }  
    render () {
        return <ExpectedAuction expectedProducts = {this.props.expectedProducts} /> 
    }
}

let mapStateToProps = (state) =>{
    return {
        expectedProducts : state.expectedAuction.expectedProducts,
    }
}


export default connect(mapStateToProps, {
    getUpcomingProducts
})(ExpectedAuctionContainer)