import React from 'react';
import classes from '../LiveAuction/LiveAuction.module.css';
import Carousel, { Dots } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

const ExpectedAuction = () => {
    
    return (
        <>
           <Carousel
                addArrowClickHandler
                autoPlay={3000}
                animationSpeed={1500}
                centered
                itemWidth={240}
                slidesPerPage={2}
                arrows
                infinite
            >
                {/* Slide 0 */}
                <div className={` ${classes.container} pb-5`}>
                    <div className={`${classes.liveauction} ${classes.clearfix}`}>
                        <div className={classes.container}>
                            <div className={classes.liveauctionblock} >
                                <div className={`${classes.liveauctionblock} ${classes.clearfix}`}>
                                    <img src={require('../../../assetc/images/Auction/auto.png')} alt="#" />
                                    <h4>Rolls Royse Phantom</h4>
                                    <div className={` p-3  ${classes.product}`}>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Start Price</span>
                                            <span className="font-12">150.000</span>
                                        </div>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Highest Bid</span>
                                            <span className="font-12 font-weight-bold">180.000</span>
                                        </div>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Time Left</span>
                                            <span className="font-12">2d:04h:15m:19s</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Slide 1 */}
                <div className={` ${classes.container} pb-5`}>
                    <div className={`${classes.liveauction} ${classes.clearfix}`}>
                        <div className={classes.container}>
                            <div className={classes.liveauctionblock} >
                                <div className={`${classes.liveauctionblock} ${classes.clearfix}`}>
                                    <img src={require('../../../assetc/images/Auction/her.png')} alt="#" />
                                    <h4>Rolls Royse Phantom</h4>
                                    <div className={` p-3  ${classes.product}`}>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Start Price</span>
                                            <span className="font-12">150.000</span>
                                        </div>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Highest Bid</span>
                                            <span className="font-12 font-weight-bold">180.000</span>
                                        </div>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Time Left</span>
                                            <span className="font-12">2d:04h:15m:19s</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Slide 2 */}
                <div className={` ${classes.container} pb-5`}>
                    <div className={`${classes.liveauction} ${classes.clearfix}`}>
                        <div className={classes.container}>
                            <div className={classes.liveauctionblock} >
                                <div className={`${classes.liveauctionblock} ${classes.clearfix}`}>
                                    <img src={require('../../../assetc/images/Auction/jam.png')} alt="#" />
                                    <h4>Rolls Royse Phantom</h4>
                                    <div className={` p-3  ${classes.product}`}>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Start Price</span>
                                            <span className="font-12">150.000</span>
                                        </div>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Highest Bid</span>
                                            <span className="font-12 font-weight-bold">180.000</span>
                                        </div>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Time Left</span>
                                            <span className="font-12">2d:04h:15m:19s</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Slide 3 */}
                <div className={` ${classes.container} pb-5`}>
                    <div className={`${classes.liveauction} ${classes.clearfix}`}>
                        <div className={classes.container}>
                            <div className={classes.liveauctionblock} >
                                <div className={`${classes.liveauctionblock} ${classes.clearfix}`}>
                                    <img src={require('../../../assetc/images/Auction/div.png')} alt="#" />
                                    <h4>Rolls Royse Phantom</h4>
                                    <div className={` p-3  ${classes.product}`}>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Start Price</span>
                                            <span className="font-12">150.000</span>
                                        </div>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Highest Bid</span>
                                            <span className="font-12 font-weight-bold">180.000</span>
                                        </div>
                                        <div className={` d-flex justify-content-between mt-3`} >
                                            <span className="font-12">Time Left</span>
                                            <span className="font-12">2d:04h:15m:19s</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </Carousel>
        
        </>
    )
}
export default ExpectedAuction