import React from 'react';
import classes from './LiveAuction.module.css' ;

const LiveAuction = (props) => {

    return (
        <div className={`${classes.container} pb-5` }>
            {props.liveproducts.map(product =>

                <div className={classes.container} key={product.id}>
                    <div className={`${classes.liveauctionblock} ${classes.clearfix}`}>
                        <img src={require('../../../assetc/images/Auction/auto.png')} alt="AUTO"/>
                        <h4>{product.title}</h4>
                        <div className={classes.product}>
                            <div className={classes.productPriceName}>
                                <h6>Auction Type</h6>
                                <h6>Start Price</h6>
                                <h6>Highest Bid</h6>
                                <h6 className={classes.timeLeft}>Time Left</h6>
                            </div>
                            <div className={` d-flex justify-content-between`} >
                                <span>Auction Type</span>
                                <span>{product.auction_type}</span>
                            </div>
                            <div className={` d-flex justify-content-between `} >
                                <span>Start Price</span>
                                <span>{product.start_price}</span>
                            </div>
                            <div className={` d-flex justify-content-between `} >
                                <span>Highest Bid</span>
                                <span>600</span>
                            </div>
                            <div className={` d-flex justify-content-between `} >
                                <span>Highest Bid</span>
                                <span>{product.end_date}</span>
                            </div>
                            <div className={classes.productPrice}>
                                <h6>{product.auction_type}</h6>
                                <h6>{product.start_price}</h6>
                                <h5>600</h5>
                                <h6>{product.end_date}</h6>
                            </div>
                        </div>
                    </div>
                </div>
      
            )}
        </div>
    )
}
export default LiveAuction