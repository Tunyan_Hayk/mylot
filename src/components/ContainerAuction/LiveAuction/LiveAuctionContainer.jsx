import React from 'react'
import { connect } from 'react-redux'
import {getLiveProducts } from '../../../redux/LiveAuctionReducer'
import LiveAuction from './LiveAuction'

class LiveAuctionContainer extends React.Component{
    componentDidMount (){
        this.props.getLiveProducts()
    }  
    render () {
        return <LiveAuction liveproducts = {this.props.liveproducts} /> 
    }
}

let mapStateToProps = (state) =>{
    return {
        liveproducts : state.liveAuction.liveproducts,
    }
}


export default connect(mapStateToProps, {
    getLiveProducts
})(LiveAuctionContainer)