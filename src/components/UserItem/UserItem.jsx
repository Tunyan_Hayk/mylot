import React from 'react';
import classes from './UserItem.module.css'
import UserAddItem from './UserAddItem/UserAddItem';

const UserItem = () =>{
    return (
        <div className={`${classes.container} ${classes.userItem}`}>
            <h3>Suggest Items</h3>
            <div className={classes.usersProduct}>
                <UserAddItem />
                <UserAddItem />
                <UserAddItem />
            </div>
            <div className={classes.userItemAdd}>
                <div className={classes.userItemAddIcon}> + </div>
                <button className={classes.results}>Results</button>
                <button className={classes.send}>Send</button>
            </div>
        </div>
    )
}
export default UserItem