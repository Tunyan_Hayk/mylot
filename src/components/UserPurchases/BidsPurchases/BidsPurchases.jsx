import React from "react";
import classes from "../UserPurchases.module.css";
import { 
    Card, 
    Row,
    Col,
    Container
} from 'react-bootstrap';

import '../UserPurchases.module.css';
import STAR from "../../../assetc/images/UserProduct/star_full.svg";
import Chrono from "../../../assetc/images/UserProduct/5pbFSsBZAiM81LhUVjZx.png";
import Hyundai from "../../../assetc/images/UserProduct/hyundai_i10_99.png";
import Fenici from "../../../assetc/images/UserProduct/fenici.png";
import DeleteModal from "../../ModalPopup/DeleteModal/DeleteModal";

const BidsPurchases = () => {
    return (
        <div className="awaiting-product pt-5">
          <Container >
            <Row className="my-4">
              <Col sm={12} lg={9}>
                <Row>
                  <Col sm={12} lg={6}>
                    <Card className="p-2">
                  <div className="d-flex justify-content-between">
                    <div className="d-flex w-40 border ">
                      <img src={Chrono} alt="5pbFSsBZAiM81LhUVjZx" className="w-100"/>
                    </div>
                    <div className="d-flex justify-content-between w-60">
                      <div className="d-flex flex-column ml-2">
                        <p className="text-left font-16">The Chrono S</p>
                        <ul className="mt-4">
                          <li className="font-12">
                            Start price: 
                            <span className="font-weight-bold">
                              1.000
                            </span>
                            <span className="font-weight-bold">$</span> 
                          </li>
                          <li class="font-12">
                            Highest suggestion: 
                            <span className="font-weight-bold"> 2.500</span>
                            <span className="font-weight-bold"> $</span>
                          </li>
                          <li class="font-12">
                            Time left: <time className="font-weight-bold">5d:14h:26m:18s</time> 
                          </li>
                          <li class="font-12">
                            Participators: <span className="font-weight-bold">5</span> 
                          </li>
                        </ul>
                     </div>
                  <div className="d-flex flex-column">
                       <DeleteModal />
                      <button className="btn btn-icon">
                      <img src={STAR} alt="STAR-FULL"/>
                      </button>
                      <button className={` btn btn-link mt-5 border bidBtn font-12 ${classes.bidBtn} `}>
                         New Bid
                      </button>
                  </div>
                </div>
                </div>
              </Card>
                  </Col>
                  <Col sm={12} lg={6}>
                    <Card className="p-2">
                  <div className="d-flex justify-content-between">
                    <div className="d-flex w-40 border ">
                      <img src={Chrono} alt="CHRONO_S" className="w-100"/>
                    </div>
                    <div className="d-flex justify-content-between w-60">
                      <div className="d-flex flex-column ml-2">
                        <p className="text-left font-16">The Chrono S</p>
                        <ul className="mt-4">
                          <li className="font-12">
                            Start price: 
                            <span className="font-weight-bold">
                              1.000
                            </span>
                            <span className="font-weight-bold">$</span> 
                          </li>
                          <li className="font-12">
                              Highest suggestion: 
                              <span className="font-weight-bold"> 2.500</span>
                              <span className="font-weight-bold"> $</span>
                          </li>
                          <li className="font-12">
                              Time left: <time className="font-weight-bold">5d:14h:26m:18s</time> 
                           </li>
                           <li className="font-12">
                             Participators: <span className="font-weight-bold">5</span> 
                            </li>
                    </ul>
                  </div>
                  <div className="d-flex flex-column">
                      <DeleteModal />
                      <button className="btn btn-icon">
                      <img src={STAR} alt="STAR-FULL"/>
                      </button>
                      <button className={` btn btn-link mt-5 border bidBtn font-12 ${classes.bidBtn} `}>
                         New Bid
                      </button>
                  </div>
                </div>
                </div>
              </Card>
                  </Col>
                </Row>  
              </Col>
              <Col sm={12} lg={3}  className="border-left">
              <h3 className="text-center header_blue">Popular Auction</h3>
                <Card className="p-3 mt-5">
                <div className="d-flex flex-column ">
                <div className="d-flex">
                  <img src={Hyundai} alt="HYUNDAI_I10_99" className="w-100"/>
                </div>
                <div className="d-flex flex-column ml-2">
                  <p className="text-center font-16">Damasca Time</p>
                  <div className="d-flex justify-content-between mt-2">
                    <span>Highest bid:</span>
                    <span className="font-weight-bold">450$</span>
                  </div>
                  <div className="d-flex justify-content-between mt-2">
                    <span>Time left</span>
                    <span className="font-weight-bold">4d:14h:8m:19s</span>
                  </div>
                  </div>
                </div>                
              </Card>
              </Col>
            </Row>
            
          </Container>
        </div>
    )
}

export default BidsPurchases