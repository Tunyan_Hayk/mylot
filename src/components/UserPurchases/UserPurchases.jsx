import React from "react";
import './UserPurchases.module.css';
import { 
    Card, 
    Tabs,
    Tab,
} from 'react-bootstrap';
import classes from './UserPurchases.module.css';
import BidsPurchases from './BidsPurchases/BidsPurchases';
import PidePurchases from './PidePurchases/PidePurchases'; 
import UnpidePurchases from './UnpidePurchases/UnpidePurchases';

const UserPurchases = () => {

    return (
        <div className="userPurchases-container container">
            <div className="pt-5">
                <Tabs defaultActiveKey="Bids" transition={false} id="noanim-tab-example" >
                    <Tab eventKey="Bids" title="Bits">
                        <Card>
                            <div className="card-product">
                                <BidsPurchases />
                            </div>
                        </Card>        
                    </Tab>
                    <Tab eventKey="Pide" title="Pide">
                        <Card>
                            <div className="card-product">
                                <PidePurchases />
                            </div>
                        </Card>
                    </Tab>
                    <Tab eventKey="Upide" title="Unpide">
                        <Card>
                            <div className="card-product">
                                <UnpidePurchases />
                            </div>
                        </Card>
                    </Tab>
                </Tabs>          
            </div>
        </div>

    )
}

export default UserPurchases