import React from 'react';
import classes from './LiveAuction.module.css' ;
import LiveAuctionBlock from './LiveAuctionBlock/LiveAuctionBlock';
import { NavLink, Link } from 'react-router-dom';
import LiveProduct from './LiveProduct/LiveProduct';
import LastChanse from './LastChanse/LastChanse';
import SuggestItem from './SuggestItem/SuggestItem';
import SuggestedAddItem from './SuggestedAddItem/SuggestedAddItem';

const LiveAuction = () => {

    return (
        <div className={` container ${classes.container} pb-5` }>
            <div className={` row `}>
                <div className={`col-lg-9`}>
                    <div className={`card `}>
                        <h3 className="text-center header_blue mt-3">Live Auction</h3>
                        <div className="col-12">
                            <div className="row">
                                <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 " >
                                    <LiveProduct />
                                </div>
                                <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 " >
                                    <LiveProduct />
                                </div>
                                <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 " >
                                    <LiveProduct />
                                </div>
                                <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 " >
                                    <LiveProduct />
                                </div>
                            </div>
                            <div className="d-flex justify-content-end py-2">
                                <Link to="/search">
                                   <span className="header_blue">View More >></span> 
                                </Link>
                            </div>
                        </div>
                    </div>
                    {/* Last Chanse */}
                    <div className={`card mt-5`}>
                        <h3 className="text-center header_blue mt-3">Last Chanse</h3>
                        <div className="col-12">
                            <div className="row">
                                <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 " >
                                    <LastChanse />
                                </div>
                                <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 " >
                                    <LastChanse />
                                </div>
                                <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 " >
                                    <LastChanse />
                                </div>
                                <div className="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3 " >
                                    <LastChanse />
                                </div>
                            </div>
                            <div className="d-flex justify-content-end py-2">
                                <Link to="/search">
                                   <span className="header_blue">View More >></span> 
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`col-lg-3`}>
                    <div className={`card`} >
                        <h3 className={` text-center header_blue mt-3 `} > Suggest items </h3>
                        <div className="">
                             <SuggestItem />
                             <SuggestedAddItem />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}
export default LiveAuction