import React from 'react'
import classes from '../Header.module.css'
import { Input } from 'antd';



const SearchNav = () =>{

    const { Search } = Input;

    return (

    <div className={`${classes.head_search} ${classes.search} d-none d-lg-block ml-3 `}>
        {/* <Search
      placeholder="input search text"
      onSearch={value => console.log(value)}
      style={{ width: 200 }}
    /> */}
        <img src={require('../../../assetc/images/Header/search.png')} width="25" height="25" alt="#"/>

    </div>
    )
}
export default SearchNav