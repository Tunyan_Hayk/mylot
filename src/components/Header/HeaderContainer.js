import React from 'react'
import {getCategoryItems,setCategory} from '../../redux/CategoryItemsReducer'
import {connect} from 'react-redux'

import Header from './Header'


class HeaderContainer extends React.Component { 

    componentDidMount () {
      
      
        this.props.getCategoryItems()

    }
    render () {
        return (
                <Header category ={this.props.category}/>
        )

    }
   

}
let mapStateToProps = (state) =>{
 
    return {
        category : state.categoryItems.category
    }
    
}
export default connect(mapStateToProps,{getCategoryItems})(HeaderContainer)