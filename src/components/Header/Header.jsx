import React, {Component} from 'react';
import classes from './Header.module.css';
import './Header.module.css';
import SearchNav from './Search/Search';
import HeaderRight from './HeaderRight/HeaderRight';
import Logo from './Logo/Logo';
import { NavLink, Link } from 'react-router-dom';
import UserIcon from './User/User';
import { Navbar, Form, Nav } from 'react-bootstrap';
import { Drawer, Divider, Menu } from 'antd';
import DrawerIcon from '../../assetc/images/Header/drawerMenu.svg';
import ARM from '../../assetc/images/Header/armenia.svg';
import RUS from '../../assetc/images/Header/russia.svg';
import USA from '../../assetc/images/Header/united-states.svg';

const { SubMenu } = Menu;

class Header extends Component {
  
  state = { visible: false };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render () {

    return (
      <div className={classes.header}>
        <Navbar className={ `justify-content-between navbar-custom`  } >
          <Nav className="w-100">
              <button type="button" 
              className={` 
                ${classes.btnDrawer} outline-none border-none drawer-menu btn-icon`} 
                onClick={this.showDrawer}>
                <img src={DrawerIcon} alt="DRAWEMENU" />
              </button>
              <Form inline>
                <SearchNav />
              </Form>
          </Nav>
          <Logo />
          <Nav className="w-100 justify-content-end">
            {/* <UserIcon /> */}
            <HeaderRight />
          </Nav>
        </Navbar>
        <Drawer
          title="MyLot"
          placement="left"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
        >
         <div className="">
         <Menu>          
          <SubMenu title="Categories" className="text-dark  font-18">
             {this.props.category.map(item =>
              <Menu.Item>
                {item.name}
              </Menu.Item>
             
            )}        
          </SubMenu>
          <Menu.Item>
            <Link to="/about_us" className="text-dark  font-18" onClick={this.onClose}>
              About
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/allItems" className="text-dark  font-18" onClick={this.onClose}>
              All Items
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/addItem" className="text-dark  font-18" onClick={this.onClose}>
              Add Item
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/favorites" className="text-dark  font-18" onClick={this.onClose}>
              Favorites
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/purchases" className="text-dark  font-18" onClick={this.onClose}>
              Purchases
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/usersettings" className="text-dark  font-18" onClick={this.onClose}>
              User Settings
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/edititem" className="text-dark  font-18" onClick={this.onClose}>
              Edit Item
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/product_live" className="text-dark  font-18" onClick={this.onClose}>
              Product Live
            </Link>
          </Menu.Item>
        </Menu>
        </div>   
        </Drawer> 
      </div>
    )
  }
}
    
export default Header