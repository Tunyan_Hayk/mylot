import React from 'react';
import '../Header.module.css';
import AddItemModal from '../../ModalPopup/AddItemModal/AddItemModal';
import { Nav } from 'react-bootstrap';
import Registers from '../Resgiter/Register';

const HeaderRight = () =>{
    return (       
      <Nav className="d-none d-xl-flex">
        <Registers />
        <Nav.Link >
          <AddItemModal />
        </Nav.Link>
      </Nav>
    )
}

export default HeaderRight