import React from 'react';
import classes from '../Header.module.css';
import { NavLink } from 'react-router-dom';

const Logo = () => {
    return (
        <div className={classes.logo}>
          <NavLink to="/auction">
            <img src={require('../../../assetc/images/Header/logo.svg')} alt="#"/>
          </NavLink>
      </div>
    )
}
export default Logo