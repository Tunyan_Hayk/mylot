import React from "react";
import { Link } from 'react-router-dom';
import { 
    Nav,
    Dropdown
} from 'react-bootstrap';


const UserIcon = () => {

    return (
        <Nav className="">
            <Dropdown>
                <Dropdown.Toggle  id="dropdown-basic">
                    <span> User </span>
                    <span>  </span> 
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item href="#/action-1">
                        <Link to="/userAuction"> My Auctions </Link>
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-2">
                        <Link to="/purchases"> My Purchases </Link>
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-3">
                        <Link to="/favorites"> Favorites </Link>
                    </Dropdown.Item>
                    <Dropdown.Item href="#/action-4">
                        <Link to="/usersettings"> Settings </Link>
                    </Dropdown.Item>
                    <Dropdown.Divider></Dropdown.Divider>
                    <Dropdown.Item href="#/action-5">
                        <Link to="/home"> Sign Out </Link>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </Nav>
    )
}

export default UserIcon