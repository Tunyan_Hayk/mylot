import React from "react"
import classes from '../Header.module.css'
import '../Header.module.css'
import {NavLink} from 'react-router-dom'

const Registers = () => {
    return (
        
        <div className={` ${classes.register_link} d-none d-lg-block `}>
          <NavLink to="/registration" className={classes.register}>REGISTER </NavLink>
          <span className={classes.reg_span}> /</span>
          <NavLink to="/login" className={classes.register}>LOG IN</NavLink>
        </div>

    )
}

export default Registers