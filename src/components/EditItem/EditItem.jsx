import React from "react";
import { 
    Card, 
    Container,
    Col,
    Row
} from "react-bootstrap";
import { Upload, Button, Divider, Select, Input, DatePicker } from 'antd';

const EditItem = () => {
      
    const props = {
        listType: 'picture',
        // defaultFileList: [...fileList],
    };

    const { Option } = Select;

    const { RangePicker } = DatePicker;

    function onChangeStart(value, dateString) {
        console.log('Start Day: ', value);
        console.log('Formatted Selected Time: ', dateString);
      }

      function onChangeEnd(value, dateString) {
        console.log('End day: ', value);
        console.log('Formatted Selected Time: ', dateString);
      }  
      
      function onOk(value) {
        console.log('onOk: ', value);
      }
      
      const { TextArea } = Input;

    return (
        <>
            <div className="additem-container pt-5">
                <Container>
                    <Card className="p-4" >
                        <Row>
                            <Col sm={12} >
                                <Divider />
                                <Upload {...props} >
                                    <div className="d-flex">
                                        <div className="" >
                                        </div>
                                    </div>
                                    <Button>
                                        Upload
                                    </Button>
                                </Upload>
                                <Divider />
                                <Row>
                                    <Col sm={6} md={3}>
                                        <span className="font-16 font-weight-bold mr-3">Auction type</span>
                                    </Col>
                                    <Col sm={6} md={9}>
                                        <Select
                                            showSearch
                                            style={{ width: 200 }}
                                            placeholder="Select a person"
                                            optionFilterProp="children"
                                            filterOption={(input, option) =>
                                                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                            }
                                        >
                                            <Option value="jack">Jack</Option>
                                            <Option value="lucy">Lucy</Option>
                                            <Option value="tom">Tom</Option>
                                        </Select>
                                    </Col>
                                </Row>
                                <Divider />
                                <Row>
                                    <Col sm={6} md={3}>
                                        <span className="font-16 font-weight-bold mr-3">Start price</span>
                                    </Col>
                                    <Col sm={6} md={9}>
                                        <div className="w-100">
                                            <Input type="number" />
                                        </div>

                                    </Col>
                                    <Col sm={6} md={3} className="mt-3">
                                        <span className="font-16 font-weight-bold mr-3">Market price</span>
                                    </Col>
                                    <Col sm={6} md={9} className="mt-3">
                                        <div className="d-flex">
                                            <Input type="number" />
                                        </div>
                                    </Col>
                                </Row>
                                <Divider />
                                <Row>
                                    <Col sm={6} md={3}>
                                        <span className="font-16 font-weight-bold mr-3">Auction type</span>
                                    </Col>
                                    <Col sm={6} md={9}>
                                        <div className="d-flex ">

                                            <DatePicker showTime onChange={onChangeStart} onOk={onOk} className="mr-3" />

                                            <DatePicker showTime onChange={onChangeEnd} onOk={onOk} className="ml-3" />
                                        </div>
                                    </Col>
                                </Row>
                                <Divider />
                                <Row>
                                    <Col sm={6} md={3}>
                                        <span className="font-16 font-weight-bold mr-3">Title</span>
                                    </Col>
                                    <Col sm={6} md={9}>
                                        <Input type="text" />
                                    </Col>
                                </Row>
                                <Divider />
                                <Row>
                                    <Col sm={6} md={3}>
                                        <span className="font-16 font-weight-bold mr-3">Description</span>
                                    </Col>
                                    <Col sm={6} md={9}>
                                        <TextArea rows={4} />
                                    </Col>
                                </Row>
                                <Divider />
                                <div className="d-flex justify-content-end">
                                    <Button type="primary" className="font-16">Save changes</Button>
                                </div>
                            </Col>
                        </Row>
                    </Card>
                </Container>
            </div>
   
        </>
    )
}

export default EditItem