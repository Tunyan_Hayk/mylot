import React from 'react';
import { Carousel } from 'react-bootstrap';
import classes from './CarouselMain.module.css';

const CarouselMain = () => {

    return (
        <div className="carousel_main" >
            <Carousel>
                <Carousel.Item className = {classes.Carousel_item}>
                    <div className = {classes.Carousel_text}>
                        
                    </div>
                </Carousel.Item>
                <Carousel.Item className = {classes.Carousel_item}>
                <div className = {classes.Carousel_text}>
                    </div>
                </Carousel.Item>
                <Carousel.Item className = {classes.Carousel_item}>
                    <div className = {classes.Carousel_text}>
                
                    </div>
                </Carousel.Item>
            </Carousel>
        </div>
    )
}

export default CarouselMain

