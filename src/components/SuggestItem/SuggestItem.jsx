import React, {Component} from 'react';
import { Radio } from 'antd';

class SuggestItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
          value: 1,
        };
      }

      onChange = e => {
        this.setState({
          value: e.target.value,
          
        });
        
      };

    render () {

        const radioStyle = {
            display: 'block',
            height: '35px',
            lineHeight: '30px',
            color : '#000000'
          };

        return (
            <div className="p-2">
                <h3 className="text-center header_blue mt-3">Suggest items</h3>
                <Radio.Group onChange={this.onChange} value={this.state.value} className="px-2">
                    <Radio style={radioStyle} value={1}>
                        <span >Real Estate</span>  
                    </Radio>
                    <Radio style={radioStyle} value={2}>
                        <span>Vehicles</span>
                    </Radio>
                    <Radio style={radioStyle} value={3}>
                        <span>Electronics</span>
                    </Radio>
                    <Radio style={radioStyle} value={4}>
                        <span>Applicances</span>
                    </Radio>
                    <Radio style={radioStyle} value={5}>
                        <span>Clothing</span>
                    </Radio>
                    <Radio style={radioStyle} value={6}>
                        <span>Culture & Hobby</span>
                    </Radio>
                    <Radio style={radioStyle} value={7}>
                        <span>Tools & Materials</span>
                    </Radio>
                    <Radio style={radioStyle} value={8}>
                        <span>Pets & Plants</span>
                    </Radio>
                </Radio.Group>  
            </div>
        );
    }  
}
  
export default SuggestItem