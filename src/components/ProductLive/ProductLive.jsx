import React from 'react';
import './ProductLive.module.css';
import { Container, Card, Row, Col, Button } from 'react-bootstrap';
import ImgOne from '../../assetc/images/UserProduct/111111@2x.png'
import ImgTwo from '../../assetc/images/UserProduct/222.png'
import ImgThree from '../../assetc/images/UserProduct/merc2.png'
import ImgFour from '../../assetc/images/UserProduct/merc3.png'
import ImgFive from '../../assetc/images/UserProduct/merc4.png'
import { Divider } from 'antd';

const ProductLive = () => {

    return (
        <>
            <div className={` productlive-container py-5`}>
                <Container>
                    <Card className="p-4">
                        <Row>
                            <Col md={12} lg={6} className={``}>
                                <div className={` row`}>
                                    <div className={`col-md-12`}>
                                        <img src={ImgOne} className={`w-100`} alt="111111@2x" />
                                    </div>        
                                </div>
                                <div className={`row mt-3`}>
                                <div className={`col-md-3`}>
                                    <img src={ImgTwo} className={`w-100`} alt="222" />
                                    </div>
                                    <div className={`col-md-3`}>
                                    <img src={ImgThree} className={`w-100`} alt="merc2" />
                                    </div>
                                    <div className={`col-md-3`}>
                                    <img src={ImgFour} className={`w-100`} alt="merc3" />
                                    </div>
                                    <div className={`col-md-3`}>
                                    <img src={ImgFive} className={`w-100`} alt="merc4" />
                                    </div>
                                </div>
                            </Col>
                            <Col md={12} lg={6} className={``}>
                                <div className={`  `}>
                                    <h3 className={` text-left font-weight-bold font-26`}>Mercedes AMG</h3>
                                    <br /> <span className={``}> Yerevan</span>
                                    <div className="">
                                        <div className={`d-flex justify-content-between`}>
                                            <div className={``}>
                                                <span>About item: </span>
                                                <span>3d 03:05:48</span>
                                            </div>
                                            <div className={``}>
                                                <span> 17/12/2019</span>
                                            </div>
                                        </div>
                                        <div className={` d-flex `}>
                                            <span> About item:</span>
                                            <p></p>
                                        </div>
                                    </div>
                                    <Divider />
                                    <div className="">
                                        <div className={`d-flex justify-content-between`}>
                                            <div className={``}>
                                                <span>Start price: </span>
                                            </div>
                                            <div className={``}>
                                                <span> 8.000$</span>
                                            </div>
                                        </div>
                                        <div className={`d-flex justify-content-between`}>
                                            <div className={``}>
                                                <span>Highest suggestion:</span>
                                            </div>
                                            <div className={``}>
                                                <span> 9.500$</span>
                                            </div>
                                        </div>
                                        <div className={`d-flex justify-content-between`}>
                                            <div className={``}>
                                                <span>Min bid: </span>
                                            </div>
                                            <div className={``}>
                                                <span> 200$</span>
                                            </div>
                                        </div>
                                        <div className={`d-flex`}>
                                            <span>Your bid:</span>
                                            <span></span>
                                        </div>
                                        <div className={`d-flex justify-content-end`}>
                                            <Button>Add to favorites</Button>
                                            <Button>Bid now</Button>
                                        </div>
                                    </div>
                                </div>
                            </Col>          
                        </Row>
                    </Card>
                    <Card className={`mt-3 p-4`} >
                        <h3 className={` font-18 text-left `}>Payment Options</h3>
                        <Row>
                            <Col sm={12} md={6}>
                                <p className={` font-14 `}>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero. Suspendisse non lacinia lectus, in gravida augue. Phasellus posuere sem sit amet ultricies ultricies. Nunc ultrices erat pretium felis laoreet gravida. Aliquam tincidunt, nullaCras id risus urna. Quisque id volutpat lectus. Vestibulum volutpat ac quam hendrerit feugiat. Sed laoreet eu tortor dictum imperdiet. Aliquam quam nulla, porta in dolor non, tempus mattis nunc. Integer dignissim condimentum neque ut suscipit. Duis ac diam eget urna euismod congue.
                                </p>
                            </Col>
                            <Col sm={12} md={6}>
                                <p className={` font-16 `}> Similar items: </p>
                                <div className={`row mt-3`}>
                                    <div className={`col-md-3`}>
                                    <img src={ImgTwo} className={`w-100`} alt="222" />
                                    </div>
                                    <div className={`col-md-3`}>
                                    <img src={ImgThree} className={`w-100`} alt="merc2" />
                                    </div>
                                    <div className={`col-md-3`}>
                                    <img src={ImgFour} className={`w-100`} alt="merc3"/>
                                    </div>
                                    <div className={`col-md-3`}>
                                    <img src={ImgFive} className={`w-100`} alt="merc4"/>
                                    </div>
                                </div>

                            </Col>
                        </Row>
                    </Card>
                </Container>
            </div>
        </>
    )
}

export default ProductLive