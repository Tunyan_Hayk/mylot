import React from 'react';
import { postRegistration } from '../../redux/AuthenticationReducer.js';
import { connect } from 'react-redux';

import Registration from './Registration'


class RegistrationContainer extends React.Component { 

    componentDidMount () {
        console.log("reg")
      
        this.props.postRegistration()

    }
    render () {
        return (
                <Registration register ={this.props.register}/>
        )

    }
   

}
let mapStateToProps = (state) =>{
    console.log("State",state)
    return {
        register : state.register.register
    }
    
}
export default connect(mapStateToProps,{postRegistration})(RegistrationContainer)