import React from 'react';
import classes from './Registration.module.css';
import './Registration.module.css';
import { Link } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import { Field, reduxForm, connect } from 'redux-form';


const Registration = (props) => {

    const { handleSubmit, pristine, reset, submitting  } = props 

    return (
        <div className={classes.Registration}>
            
            <div className="refister-container">
                <div className="row ">
                    <div className={`register-img col-md-6 d-none d-md-block `} >
                        
                        <div className={classes.register_img}>
                        <img src={require('../../assetc/images/Registration/left_bg.png')} alt="#" />
                        <div className={classes.Img_tex}>
                            <h3 className="font-30 text-white">Create your</h3>
                            <h4 className="font-30 text-white">Account</h4>
                            <p className="font-25 text-white">And make your first bid</p>
                        </div>
                    </div>
                    </div>
                    <div className="login-container col-md-6 mt-5">
                        <div className="row ">
                            <div className="col-12 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                                <div className=" p-2 d-md-flex justify-content-md-center">
                                    <Form onSubmit={handleSubmit} >
                                        <div className="d-flex flex-column">
                                            <h2 className="text-center font-weight-bold font-30"> Registration</h2>
                                            <p className=" text-center font-20"> Already have an account?
                                                <Link className="mx-3 font-weight-bold text-black text-dark" to="/login">
                                                    Log In
                                                </Link>
                                            </p>
                                        </div>
                                        {/* <Field placeholder={"Registr"} component={"input"} /> */}

                                        <Form.Group controlId="formBasicFirstName">
                                            <Form.Control type="text" placeholder="First Name" className={classes.control} />

                                        </Form.Group>
                                        <Form.Group controlId="formBasicLastName">
                                            <Form.Control type="text" placeholder="Last Name" className={classes.control} />
                                        </Form.Group>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Control type="email" placeholder="Email" className={classes.control} />
                                        </Form.Group>
                                        <Form.Group controlId="formBasicPhone">
                                            <Form.Control type="number" placeholder="Phone number" className={classes.control} />
                                        </Form.Group>
                                        <Form.Group controlId="formBasicPassword">
                                            <Form.Control type="password" placeholder="Password" className={classes.control} />
                                        </Form.Group>
                                        <Form.Group controlId="formBasicRepassword">
                                            <Form.Control type="password" placeholder="Re-enter password" className={classes.control} />
                                        </Form.Group>
                                        <Button variant="primary" disabled={pristine || submitting} type="submit" className="btn-block btn-login mt-4 text-white">
                                            <Link to="/home" className="text-white">Log In</Link>
                                        </Button>
                                    </Form>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}



export default Registration