import React from "react";
import { 
    Card, 
    Row,
    Col,
    Container
} from 'react-bootstrap';
import { Divider } from 'antd';
import "./Favorites.module.css";
import classes from './Favorites.module.css';
import Favorit from "../../assetc/images/UserProduct/favorite_full.svg";
import DELETE from "../../assetc/images/UserProduct/delete.svg";
import Chrono from "../../assetc/images/UserProduct/Chrono-S.png";
import TabSamsung from "../../assetc/images/UserProduct/samsung-tab-s4.png";
import Jam from "../../assetc/images/Auction/jam.png";


const Favorites = () => {

    return (
        <div className="favorites-product pt-5">
          <Container className="card" >
            <Row className="my-4">
              <Col sm={12} lg={9}>
                <Row>
                  <Col sm={12} lg={4}>
                  <Card className="p-3">
                    <div className="d-flex flex-column">
                      <div className="d-flex w-100 ">
                        <img src={Jam} alt="JAM" className="fav-img w-100" />

                      </div>
                      <span><img src={Favorit} alt="FAVORITE-FULL" className={classes.FavIcon} /></span>
                      <div className="d-flex flex-column ml-2">
                        <p className="text-center font-16">Damasca Time</p>
                        <div className="d-flex justify-content-between mt-2">
                          <span>Highest bid:</span>
                          <span className="font-weight-bold">450$</span>
                        </div>
                        <div className="d-flex justify-content-between mt-2">
                          <span>Time left</span>
                          <span className="font-weight-bold">4d:14h:8m:19s</span>
                        </div>
                      </div>
                    </div>
                  </Card>
                  </Col>
                  <Col sm={12} lg={4}>
                    <Card className="p-3">
                    <div className="d-flex flex-column">
                      <div className="d-flex w-100 ">
                        <img src={Jam} alt="JAM" className="fav-img w-100" />
                      </div>
                      <span><img src={Favorit} alt="FAVORITE-FULL" className={classes.FavIcon} /></span>
                      <div className="d-flex flex-column ml-2">
                        <p className="text-center font-16">Damasca Time</p>
                        <div className="d-flex justify-content-between mt-2">
                          <span>Highest bid:</span>
                          <span className="font-weight-bold">450$</span>
                        </div>
                        <div className="d-flex justify-content-between mt-2">
                          <span>Time left</span>
                          <span className="font-weight-bold">4d:14h:8m:19s</span>
                        </div>
                      </div>
                    </div>
                    </Card>
                  </Col>
                  <Col sm={12} lg={4}>
                    <Card className="p-3">
                    <div className="d-flex flex-column">
                      <div className="d-flex w-100 ">
                        <img src={Jam} alt="JAM" className="fav-img w-100" />
                      </div>
                      <span><img src={Favorit} alt="FAVORITE-FULL" className={classes.FavIcon} /></span>
                      <div className="d-flex flex-column ml-2">
                        <p className="text-center font-16">Damasca Time</p>
                        <div className="d-flex justify-content-between mt-2">
                          <span>Highest bid:</span>
                          <span className="font-weight-bold">450$</span>
                        </div>
                        <div className="d-flex justify-content-between mt-2">
                          <span>Time left</span>
                          <span className="font-weight-bold">4d:14h:8m:19s</span>
                        </div>
                      </div>
                    </div>
                 </Card>
                  </Col>
                </Row>  
              </Col>
              
              <Col sm={12} lg={3} className="border-left">
                <h3 className="text-center header_blue">Popular Auction</h3>
                <Card className="p-3 mt-5">
                <div className="d-flex flex-column ">
                <div className="d-flex">
                  <img src={TabSamsung} alt="SAMSUNG_TAB_S4" className="w-100"/>
                </div>
                <div className="d-flex flex-column ml-2">
                  <p className="text-center font-16">Damasca Time</p>
                  <div className="d-flex justify-content-between mt-2">
                    <span>Highest bid:</span>
                    <span className="font-weight-bold">450$</span>
                  </div>
                  <div className="d-flex justify-content-between mt-2">
                    <span>Time left</span>
                    <span className="font-weight-bold">4d:14h:8m:19s</span>
                  </div>
                  </div>
                </div>                
              </Card>      
              </Col>
            </Row>          
          </Container>
        </div>
    )
}

export default Favorites