import React from 'react';
import { Link } from "react-router-dom";
import classes from './Footer.module.css' ;
import logo from '../../assetc/images/Login/Logo.png'
import BestAuction from '../../assetc/images/Login/Best_Auction.png'
import facebook from '../../assetc/images/Footer/facebook.png'
import instagram from '../../assetc/images/Footer/instagram.png'
import google from '../../assetc/images/Footer/google.png'
import location from '../../assetc/images/Footer/location-pin.png'
import mail from '../../assetc/images/Footer/mail.png'
import phone from '../../assetc/images/Footer/phone-call.png'

import { ListGroup } from 'react-bootstrap'

const Footer = () => {
    return (
        <div className={` ${classes.footer} py-5` }>
            <div className="container">
                <div className={` row `} >
                <div className={` col-12 col-md-12 col-lg-6`} >
                    <div className={classes.logo}>
                        <img src={logo} alt="#"/>
                        <img src={BestAuction} alt="#"/>
                    </div>
                    <p className={`text-text font-12 w-50`}>
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, 
                        sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, 
                        sed diam voluptua.
                    </p>
                    <div className={` favicon-link `}>
                        <div className={``}>
                            <img className={` ml-3  `} src={facebook} alt="FACEBOOK" />
                            <img className={` ml-3  `} src={instagram} alt="INSTAGRAM" />
                            <img className={` ml-3  `} src={google} alt="GOOGLE" />
                        </div>
                    </div>
                </div>
                <div className={` col-12 col-md-6  col-lg-3`} >
                    <ListGroup className={`border-0`}>
                        <ListGroup.Item className={`border-0`}>
                            <Link to="/home">Home</Link>
                        </ListGroup.Item>
                        <ListGroup.Item className={`border-0`}>
                            <Link to="/about_us">About Us</Link>
                        </ListGroup.Item>
                        <ListGroup.Item className={`border-0`}>
                            <Link to="/">Privacy Policy</Link>
                        </ListGroup.Item>
                        <ListGroup.Item className={`border-0`}>
                            <Link to="/">Terms of Use</Link>
                        </ListGroup.Item>
                    </ListGroup>
                </div>
                <div className={` col-12 col-md-6 col-lg-3`} >
                    <ListGroup className={`border-0`}>
                        <ListGroup.Item className={`border-0`}>
                            <Link to="/contact_us">Contact Us</Link>   
                        </ListGroup.Item>
                        <ListGroup.Item className={`border-0`}>
                            <img className={` mr-2  `} width="20" src={location} alt="LOCATION_PIN" />
                            <span>Address</span>  
                        </ListGroup.Item>
                        <ListGroup.Item className={`border-0`}>
                            <img className={` mr-2  `} width="20" src={mail} alt="MAIL" />
                            <span>yourmail@gmail.com</span> 
                        </ListGroup.Item>
                        <ListGroup.Item className={`border-0`}>
                            <img className={` mr-2  `} width="20" src={phone} alt="PHONE_CALL" />
                            <span>+374 99 00 00 00</span>  
                        </ListGroup.Item>
                    </ListGroup>
                </div>
                </div>
            </div>
        </div>
    )
}

export default Footer