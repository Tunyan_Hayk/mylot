import React from 'react';
import classes from './UserAuction.module.css'
import './UserAuction.module.css'
import LiveProduct from './LiveProduct/LiveProduct'
import { Link } from 'react-router-dom';
import { 
    Card, 
    Tabs,
    Tab,
    Button
 } from 'react-bootstrap';
import AwaitingProduct from './AwaitingProduct/AwaitingProduct';

const UserAuction = () => {

    return (
        <div className="userAuction_container container">
            <div className="pt-5">
                <Tabs defaultActiveKey="Live" transition={false} id="noanim-tab-example" >
                    <Tab eventKey="Live" title="Live">
                        <Card >
                            <Card.Body className="d-flex justify-content-center">
                                <div className={classes.addBtn}>
                                    <Button variant="primary" block className="font-16 text-center btn-block">
                                        <Link to="/addItem" className="text-white">ADD ITEM</Link>  
                                    </Button>
                                </div>
                                
                            </Card.Body>
                            <div className="card-product">
                                <LiveProduct />
                            </div>
                        </Card>        
                    </Tab>
                    <Tab eventKey="Awaiting" title="Awaiting">
                        <Card>
                        <Card.Body className="d-flex justify-content-center">
                                <div className={classes.addBtn}>
                                    <Button variant="primary" block className="font-16 text-center btn-block">
                                        <Link to="/addItem" className="text-white">ADD ITEM</Link> 
                                    </Button>
                                </div>
                                
                            </Card.Body>
                            <div className="card-product">
                                <AwaitingProduct />
                            </div>
                        </Card>
                    </Tab>
                </Tabs>
                
            </div>
        </div>
    )
}

export default UserAuction
