import React from "react";
import { 
    Card, 
    Row,
    Col,
    Container
} from 'react-bootstrap';
import { Link } from "react-router-dom";
 import DeleteModal from "../../ModalPopup/DeleteModal/DeleteModal";
 import ChooseDateModal from "../../ModalPopup/ChooseDateModal/ChooseDateModal";
 import EDIT from "../../../assetc/images/UserProduct/edit.svg";
 import DELETE from "../../../assetc/images/UserProduct/delete.svg";
 import Chrono from "../../../assetc/images/UserProduct/Chrono-S.png";
 import TabSamsung from "../../../assetc/images/UserProduct/samsung-tab-s4.png";
 import Fenici from "../../../assetc/images/UserProduct/fenici.png";
 import classes from '../UserAuction.module.css';
 import '../UserAuction.module.css';

const AwaitingProduct = () => {
    return (
        <div className="awaiting-product pt-5">
          <Container >
            <Row className="my-4">
              <Col sm={12} lg={6}>
                <Card className={` p-3  ${classes.cardProduct} `}>
                  <div className="d-flex justify-content-around">
                    <div className="d-flex w-40 border ">
                      <img src={Chrono} alt="CHRONO_S"/>
                    </div>
                    <div className="d-flex justify-content-between w-60">
                      <div className="d-flex flex-column ml-2">
                        <p className="text-left font-16">The Chrono S</p>
                        <ul class="mt-4">
                          <li class="">
                            Start price: 
                            <span className="font-weight-bold">
                              1.000
                            </span>
                            <span className="font-weight-bold">$</span> 
                          </li>
                      <li class="">
                        Highest suggestion: <span className="font-weight-bold"> 2.500</span><span className="font-weight-bold"> $</span>
                      </li>
                      <li class="">
                        Time left: <time className="font-weight-bold">5d:14h:26m:18s</time> 
                      </li>
                      <li class="">
                        Participators: <span className="font-weight-bold">5</span> 
                      </li>
                    </ul>
                  </div>
                  <div className="d-flex flex-column">
                      <Link className="btn btn-icon" to="/edititem">
                        <img src={EDIT} alt="EDIT"/>
                      </Link>
                      <DeleteModal />
                      <ChooseDateModal />
                  </div>
                </div>
                </div>
              </Card>      
              </Col>
              <Col sm={12} lg={6}>
              <Card className={` p-3  ${classes.cardProduct} `}>
                <div className="d-flex justify-content-around">
                <div className="d-flex w-40 border">
                  <img src={TabSamsung} alt="SAMSUNG_TAB_S4"/>
                </div>
                <div className="d-flex justify-content-between w-60">
                  <div className="d-flex flex-column ml-2">
                    <p className="text-left font-16">Tablet Samsung S4</p>
                    <ul class="mt-4">
                      <li class="">
                      Start price:<span className="font-weight-bold"> 1000 </span> <span className="font-weight-bold">$</span> 
                      </li>
                      <li class="">
                      Highest suggestion:<span className="font-weight-bold">  2.500 </span><span className="font-weight-bold">$</span>
                      </li>
                      <li class="">
                      Time left: <time className="font-weight-bold">5d:14h:26m:18s</time> 
                      </li>
                      <li class="">
                        Participators: <span className="font-weight-bold">5</span> 
                      </li>
                    </ul>
                  </div>
                  <div className="d-flex flex-column">
                      <Link className="btn btn-icon" to="/edititem">
                        <img src={EDIT} alt="EDIT"/>
                      </Link>
                      <DeleteModal />
                      <ChooseDateModal />
                  </div>
                </div>
                </div>                
              </Card>      
              </Col>
            </Row>
            <Row className="my-4">
              <Col sm={12} lg={6}>
              <Card className={` p-3  ${classes.cardProduct} `}>
                <div className="d-flex justify-content-around">
                <div className="d-flex w-40 border">
                  <img src={Fenici} alt="FENICI"/>
                </div>
                <div className="d-flex justify-content-between w-60">
                  <div className="d-flex flex-column ml-2">
                    <p className="text-left font-16">Fenici 30cm Desk Fan</p>
                    <ul class="mt-4">
                      <li class="">
                        Start price: <span className="font-weight-bold">1000</span> <span className="font-weight-bold">$</span> 
                      </li>
                      <li class="">
                        Highest suggestion: <span className="font-weight-bold"> 2.500</span><span className="font-weight-bold"> $</span>
                      </li>
                      <li class="">
                        Time left: <time className="font-weight-bold">5d:14h:26m:18s</time> 
                      </li>
                      <li class="">
                        Participators: <span className="font-weight-bold">5</span> 
                      </li>
                    </ul>
                  </div>
                  <div className="d-flex flex-column">
                      <Link className="btn btn-icon" to="/edititem">
                        <img src={EDIT} alt="EDIT"/>
                      </Link>
                      <DeleteModal />
                      <ChooseDateModal />
                  </div>
                </div>
                </div>
              </Card>      
              </Col>
            </Row>
          </Container>
        </div>
    )
}

export default AwaitingProduct