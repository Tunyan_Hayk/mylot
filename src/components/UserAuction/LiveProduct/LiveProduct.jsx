import React from "react";
import { 
    Card, 
    Row,
    Col,
    Container
 } from 'react-bootstrap';
 import DeleteModal from "../../ModalPopup/DeleteModal/DeleteModal";
 import { Link } from "react-router-dom";
 import Hyundai from "../../../assetc/images/UserProduct/hyundai_i10_99.png";
 import EDIT from "../../../assetc/images/UserProduct/edit.svg";
 import DELETE from "../../../assetc/images/UserProduct/delete.svg";
 import Bosch from "../../../assetc/images/UserProduct/l_10163128_002.png";
 import Apple from "../../../assetc/images/UserProduct/apple-iphone-8-128gb-gold.png";
 import classes from '../UserAuction.module.css';

const LiveProduct = () => {

    return (
        <div className="user_product pt-5">
          <Container >
            <Row className="my-4">
              <Col sm={12} lg={6}>
                <Card className={` p-3  ${classes.cardProduct} `}>
                  <div className="d-flex justify-content-around">
                    <div className="d-flex w-40 border">
                      <img src={Hyundai} alt="HYUNDAI_I10_99"/>
                    </div>
                    <div className="d-flex justify-content-between w-60">
                      <div className="d-flex flex-column ml-2">
                        <p className="text-left font-16">Hyundai i10</p>
                        <ul class="mt-4">
                          <li class="">
                            Start price: 
                            <span className="font-weight-bold">
                              5.000
                            </span>
                            <span className="font-weight-bold">$</span> 
                          </li>
                      <li class="">
                        Highest suggestion: <span className="font-weight-bold"> 7.500</span><span className="font-weight-bold"> $</span>
                      </li>
                      <li class="">
                        Time left: <time className="font-weight-bold">5d:14h:26m:18s</time> 
                      </li>
                      <li class="">
                        Participators: <span className="font-weight-bold">17</span> 
                      </li>
                    </ul>
                  </div>
                  <div className="d-flex flex-column">
                      <Link className="btn btn-icon" to="/edititem">
                        <img src={EDIT} alt="EDIT"/>
                      </Link>
                      <DeleteModal />                    
                  </div>
                </div>
                </div>
              </Card>      
              </Col>
              <Col sm={12} lg={6}>
              <Card className={` p-3  ${classes.cardProduct} `}>
                <div className="d-flex justify-content-around">
                <div className="d-flex w-40 border">
                  <img src={Bosch} alt="l_10163128_002"/>
                </div>
                <div className="d-flex justify-content-between w-60">
                  <div className="d-flex flex-column ml-2">
                    <p className="text-left font-16">BOSCH Serie 4</p>
                    <ul class="mt-4">
                      <li class="">
                      Start price:<span className="font-weight-bold"> 500 </span> <span className="font-weight-bold">$</span> 
                      </li>
                      <li class="">
                      Highest suggestion:<span className="font-weight-bold">  1.000 </span><span className="font-weight-bold">$</span>
                      </li>
                      <li class="">
                      Time left: <time className="font-weight-bold"> 8d:1h:52m:11s</time> 
                      </li>
                      <li class="">
                        Participators: <span className="font-weight-bold">6</span> 
                      </li>
                    </ul>
                  </div>
                  <div className="d-flex flex-column">
                      <Link className="btn btn-icon" to="/edititem">
                        <img src={EDIT} alt="EDIT"/>
                      </Link>
                      <DeleteModal />               
                  </div>
                </div>
                </div>                
              </Card>      
              </Col>
            </Row>
            <Row className="my-4">
              <Col sm={12} lg={6}>
              <Card className={` p-3  ${classes.cardProduct} `}>
                <div className="d-flex justify-content-around">
                <div className="d-flex w-40 border">
                  <img src={Apple} alt="APPLE-IPHONE-8-128GB-GOLD"/>
                </div>
                <div className="d-flex justify-content-between w-60">
                  <div className="d-flex flex-column ml-2">
                    <p className="text-left font-16">Apple iPhone 8 Gold</p>
                    <ul class="mt-4">
                      <li class="">
                        Start price: <span className="font-weight-bold">500</span> <span className="font-weight-bold">$</span> 
                      </li>
                      <li class="">
                        Highest suggestion: <span className="font-weight-bold"> 1.000</span><span className="font-weight-bold"> $</span>
                      </li>
                      <li class="">
                        Time left: <time className="font-weight-bold">8d:1h:52m:11s</time> 
                      </li>
                      <li class="">
                        Participators: <span className="font-weight-bold">14</span> 
                      </li>
                    </ul>
                  </div>
                  <div className="d-flex flex-column">
                      <Link className="btn btn-icon" to="/edititem">
                        <img src={EDIT} alt="EDIT"/>
                      </Link>
                      <DeleteModal />          
                  </div>
                </div>
                </div>
              </Card>      
              </Col>
            </Row>
          </Container>
        </div>
    )
}

export default LiveProduct