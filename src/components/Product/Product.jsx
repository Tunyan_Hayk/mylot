import React from 'react';

import classes from './Product.module.css'

const Product = () => {
    
    return (
        <div className={classes.Product}>
            <div className={classes.product_item}>
                <div className={classes.product_img}>
                    <img src={require('../../img/Auction/auto.png')} alt="#"/>
                </div>
                
                <div className={classes.productPage}>
                    <div className={`${classes.product_bid} ${classes.clearfix}`}>
                        <h4>Mercedes AMG</h4>
                        <h6>Yerevan</h6>
                        <div className={classes.bid_time}>
                            <p>Time left:</p>
                            <span>3d 03:05:48</span>
                        </div>
                        <p className={classes.bid_item_date}>17/12/2019</p>
                        <p className={classes.bid_item_about}>About item:</p>
                    </div>
                    <div className={`${classes.product_price_item} ${classes.clearfix}`}>
                        <div className={classes.product_price}>
                            <p>Start price</p>
                            <p>Highest suggestion</p>
                            <p>Min bid</p>
                            <p>Your bid</p>
                        </div>
                        <div className={classes.product_item_price}>
                            <p>8.000$</p>
                            <p>9.500$</p>
                            <p>200</p>
                        </div>
                    </div>
                </div>
               
            </div>
           
           
        </div>
    )
}

export default Product