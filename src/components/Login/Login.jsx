import React from 'react';
import classes from './Login.module.css';
import { Link } from 'react-router-dom';
import LoginTop from '../../assetc/images/Register/PathAccount.svg'
import { Checkbox } from 'antd';
import RessetPassword from "../ModalPopup/RessetPassword/RessetPassword"
import { Form, Button, Col } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';


function onRemember(e) {
    console.log(`checked = ${e.target.checked}`);
}

const Login = (props) => {

    console.log("props login ", props);
    const { handleSubmit, pristine, reset, submitting } = props


    return (  
    <div className={classes.Login}>

        <div className="refister-container">
            <div className="row ">
            <div className={`register-img col-md-6 d-none d-md-block `} >
                <div className="d-none d-md-block ">
                    <img src={LoginTop} className={classes.registerTop}  alt="PATHACCOUNT"/>
                </div>
            </div>
            <div className="login-container col-md-6 mt-5">      
                <div className="row mt-5">
                    <div className="col-12 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                    <div className="d-flex flex-column">
                    <h2 className="text-center font-weight-bold font-30"> Log In </h2>
                    <p className=" text-center font-20"> Don't have an account? 
                        <Link className="mx-3 font-weight-bold text-black text-dark" to="/registration">Sign Up</Link> 
                    </p>
                </div>
                <Form onSubmit={handleSubmit}>
                    <Form.Group controlId="formBasicEmail">
                        <Field component={"input"} type="email" name="email" placeholder="Email" className={` form-control ${classes.control} `  } />
                        
                       
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword">
                        <Field component={"input"} type="password" name="password" placeholder="Password" className={` form-control ${classes.control} `  } />
                        
                        {/* <Form.Control type="password" placeholder="Password" className={classes.control} /> */}
                    </Form.Group>
                        <Form.Row>
                            <Col>
                                {/* <Field component={"checkbox"} onChange={onRemember} /> {' '}
                                <span>Remember Me</span> */}
                                <Checkbox onChange={onRemember}>Remember me</Checkbox>
                            </Col>
                            <Col>
                            <RessetPassword />
                            </Col>
                        </Form.Row>         
                    <Button disabled={pristine || submitting} variant="primary" type="submit" className="btn-block btn-login mt-4 text-white">
                        <Link to="/home" className="text-white">Log In</Link>   
                    </Button>
                </Form>
                    </div>
                </div>  
            </div>
        </div>
        </div>
        
        </div>
    )
}



export default reduxForm({
    form: "login"
}) (Login)