import React from 'react';
import { postLogin } from '../../redux/LoginReducer.js';
import { connect } from 'react-redux';

import Login from './Login'


class LoginContainer extends React.Component { 

    componentDidMount () {
              
        this.props.postLogin()

    }
    render () {
        return (
                <Login login ={this.props.login}/>
        )

    }
   
}

let mapStateToProps = (state) =>{
    
    return {
        login : state.login.login
    }
    
}
export default connect(mapStateToProps,{postLogin})(LoginContainer)