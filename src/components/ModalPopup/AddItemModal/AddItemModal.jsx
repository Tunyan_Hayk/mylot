import React, {useState} from 'react';
import classes from '../ModalPopup.module.css';
import { NavLink } from 'react-router-dom';
import { Modal } from 'react-bootstrap';

const AddItemModal = () => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>

        <button onClick = {handleShow} className={classes.AddItem}>Add Item</button>
  
        <Modal show={show} onHide={handleClose} animation={false}>
          <Modal.Header className="border-0" closeButton > 
          </Modal.Header>
          <Modal.Body className="border-none">
            <div className="">
              <div className="">
                <h3 className="text-center font-24">Want to add an item?</h3>
                <h3 className="text-center font-weight-bold font-24">Please sign in to continue</h3>
              </div>
              <div className="d-flex flex-column p-5">
                <NavLink className={` ${classes.AddItem_btn} font-18 mb-4`} onClick = { handleClose } to="/login">
                  LOGIN NOW
                </NavLink>
                <div className="d-flex flex-row">
                  <span className={` ${classes.modal_border} `}></span>
                  <span className="font-20 mx-3">or</span>
                  <span className={` ${classes.modal_border} `}></span>
                </div>
                <NavLink className={` ${classes.AddItem_btn} font-18 mt-4`} onClick = { handleClose } to="/login">
                  CREATE YOUR ACCOUNT
                </NavLink>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </>
    )
}
export default AddItemModal