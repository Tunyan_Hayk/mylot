import React, {useState} from 'react';
import classes from '../ModalPopup.module.css';
import { 
  Modal, 
  Button
} from 'react-bootstrap';
import DELETE from "../../../assetc/images/UserProduct/delete.svg";

const DeleteModal = () => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
        {/* <button onClick = {handleShow} className={classes.AddItem}>Delete Modal</button> */}
        <button onClick = {handleShow} className={`btn btn-icon ${classes.Delete_modal}` }>
            <img src={DELETE} alt="DELETE"/>
        </button>
        <Modal show={show} onHide={handleClose} animation={false}>
          <Modal.Header className="border-0">
            <Modal.Title className="w-100 text-center font-24">Delete selected item?</Modal.Title>
          </Modal.Header>
          <Modal.Body className="border-0 font-14 text-center">Are you sure you want to delete this item?</Modal.Body>
          <Modal.Footer className="border-0 d-flex justify-content-around">
            <Button variant="secondary" onClick={handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={handleClose}>
              Delete
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    )
}
export default DeleteModal