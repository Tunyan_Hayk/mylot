import React from 'react';
import classes from './SuggestedAddItem.module.css';

const SuggestedAddItem = () => {

    return (
        <div className="p-2">
            <div className={`${classes.suggest_addItem} ${classes.clearfix}`}>
                <div className={classes.suggest_add}>+</div>
                <div className={classes.results}>
                    <button>Results</button>
                </div>
                <div className={classes.send}>
                    <button>Send</button>
                </div>
            </div>
        </div>
    )
}

export default SuggestedAddItem
