import React from 'react';
import './ContactUs.module.css';
import classes from './ContactUs.module.css';
import { 
    Col,
    Row,
    Form,
    Button,
} from "react-bootstrap";
import { Input } from 'antd';
import AboutTop from '../../assetc/images/AboutUs/About_top.png';
import AboutUnder from '../../assetc/images/AboutUs/About_under.png';

const ContactUs = () => {

    const { TextArea } = Input;

    return (
        <>
            <div className={`contactus-container `}>
            <img src={AboutTop} alt="#" />
                <div className={` container `}>
                    <div className={`row`}>
                         <div className={` col-12 col-md-6 offset-md-3 `}>
                         <h3 className={` text-center font-22 `}>Contact Us</h3>
                        <p className={` text-center  font-16 mt-5`}>
                          If you have any comments or feedback, please do not hesitate to write us. We love feedback!
                        </p>
                    <Form className={` mt-5 `}>
                        <Form.Group as={Row} controlId="formName">
                            <Form.Label column sm="4" className={` font-weight-bold `}>
                               Name
                            </Form.Label>
                            <Col sm="8">
                                <Form.Control type="text" />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="formEmail" className={` mt-3 `}>
                            <Form.Label column sm="4" className={` font-weight-bold `}>
                                Email
                            </Form.Label>
                            <Col sm="8">
                                <Form.Control type="email" />
                            </Col>
                        </Form.Group> 
                        <Form.Group as={Row} controlId="formPhone" className={` mt-3 `}>
                            <Form.Label column sm="4" className={` font-weight-bold `}>
                                Phone Number
                            </Form.Label>
                            <Col sm="8">
                                <Form.Control type="number" />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="formMessage" className={` mt-3 `}>
                            <Form.Label column sm="4" className={` font-weight-bold `}>
                                Message
                            </Form.Label>
                            <Col sm="8">
                               <TextArea rows={5} />
                            </Col>
                        </Form.Group>
                        <Form.Group className={` d-flex justify-content-end mt-3`}>
                            <Button className={` btn px-4 w-25 font-14`}> Send </Button>
                        </Form.Group>
                    </Form>
                        </div>
                    </div>
                </div>
                <div className={` ${classes.ContactImgtwo} `}>
                <img src={AboutUnder} alt="#" className={classes.ContactUnder}/>
            </div>
            </div>
        </>
    )
}

export default ContactUs