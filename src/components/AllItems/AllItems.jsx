import React from 'react';
import classes from './AllItems.module.css';
import AllItemsFilter from './AllItemsFilter/AllItemsFilter';
import AuctionItemContainer from './AuctionItem/AuctionItemContainer';

const AllItems = () => {

    return(
        <div className={` mt-5 ${classes.AllItems} `}>
            <AllItemsFilter />
            <div className={` ${classes.auction_item} container `  }>
                <div className={`${classes.AuctionItemAll} ${classes.clearfix} `}>
                    <AuctionItemContainer />
                </div>
            </div>
        </div>
    )
}
         
export default AllItems