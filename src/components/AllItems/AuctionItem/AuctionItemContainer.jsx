import React from 'react';
import { connect } from 'react-redux';
import { getLiveProducts } from '../../../redux/LiveAuctionReducer';
import AuctionItem from './AuctionItem'

class AuctionItemContainer extends React.Component{
    componentDidMount (){
        this.props.getLiveProducts()
    }  
    render () {
        return <AuctionItem liveproducts = {this.props.liveproducts} /> 
    }
}


let mapStateToProps = (state) =>{
    return {
        liveproducts : state.liveAuction.liveproducts,
    }
}


export default connect(mapStateToProps, {
    getLiveProducts
})(AuctionItemContainer)