import React from "react";
import classes from "../AddItem.module.css" ;
import { 
    Col,
    Row
  } from "react-bootstrap";
  import { Link } from "react-router-dom";
  import Itemimg from "../../../assetc/images/UserProduct/111111@2x.png";

const ItemFinish = () => {

    return (
        <>
            <Row>
            <Col sm={12} lg={4}>
                <div className={` mt-5 `}>
                    <h3 className={` font-18 `}>Post has been published</h3>
                    <div className={` `}>
                        <img src={Itemimg} alt=""/>
                    </div>
                </div>
            </Col>
            <Col sm={12} lg={8}>
                <div className={` mt-5 `}>
                    <div className="d-flex flex-column">
                        <p className={` font-18 mt-4`}>Mercedes Benz E53</p>
                        <p className={`font-18 `}> 5000$</p>
                        <div className="mt-5">
                            <Link to="/userAuction" className={`btn text-white ${classes.finshBtn} `}>My auctions </Link> 
                            <br />
                            <Link to="/addItem" className={`btn text-white mt-2  ${classes.finshBtn} `}> Add new item </Link>
                        </div>
                    </div>
                </div>
            </Col>
        </Row>
    </>
    )
}

export default ItemFinish