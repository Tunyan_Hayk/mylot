import React, { Component } from 'react';
import { 
    getCategoryItems , 
    setCategory 
} from '../../../redux/CategoryItemsReducer';

import { connect } from 'react-redux';

import ItemCategory from './ItemCategory';

class ItemCategoryContainer extends Component { 

    componentDidMount () {
        this.props.getCategoryItems()
    }

    render () {
        return (
                <ItemCategory category ={this.props.category}/>
        )
    }
}

let mapStateToProps = (state) => {
    
    return {

        category : state.categoryItems.category
    }
   
}

export default connect(mapStateToProps,{getCategoryItems})(ItemCategoryContainer)