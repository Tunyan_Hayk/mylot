import React from "react";
import { 
    Col,
    Row,
    Nav
  } from "react-bootstrap";

  import {  Menu } from 'antd';

const ItemCategory = (category) => {

    const { SubMenu } = Menu;

    return (
        <>
        <Row>
            <Col sm={12} lg={3}>
                <div className={` mt-5 `}>
                    <h3 className={` font-20 `}>Choose category</h3>
                    <Nav className= {`flex-column`}>
                        {category.category.map(item => 
                           <Nav.Link>
                               {item.name}
                           </Nav.Link>
                        )}
                    </Nav>
                </div>
            </Col>
            <Col sm={12} lg={9}>
                <div className={` `}>
                        
                </div>
            </Col>
        </Row>        
        </>
    )
}

export default ItemCategory