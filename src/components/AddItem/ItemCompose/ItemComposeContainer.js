import React, { Component } from 'react';
import { 
    postAddProdcut , 
    setProduct 
} from '../../../redux/AddProductReducer';

import { connect } from 'react-redux';

import ItemCompose from './ItemCompose';

class ItemComposeContainer extends Component { 

    componentDidMount () {
        this.props.postAddProdcut()
    }

    render () {
        return (
                <ItemCompose product ={this.props.product}/>
        )
    }
}

let mapStateToProps = (state) =>{
    
    return {

        product : state.addProduct.product
    }
   
}

export default connect( 
    mapStateToProps,
    {postAddProdcut}
    )
    (ItemComposeContainer)