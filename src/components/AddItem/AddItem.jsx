import React from "react";
import "./AddItem.module.css";
import { Tabs, Steps } from 'antd';
import { 
  Card, 
  Container
} from "react-bootstrap";
import ItemCompose from "./ItemCompose/ItemCompose";
import ItemFinish from "./ItemFinish/ItemFinish";
import ItemCategoryContainer from "./ItemCategory/ItemCategoryContainer";

const AddItem = () => {

  const { TabPane } = Tabs;

  const { Step } = Steps;

  return (
    <>
      <div className="additem-container pt-5">
        <Container>
          <Card className="p-4" >
          <Steps>
              <Step status="finish" title="Categories"  />
              <Step status="finish" title="Compose" />            
              <Step status="finish" title="Finish" />
            </Steps>
            <Tabs defaultActiveKey="1">
              <TabPane tab="Categories" key="1">
                <ItemCategoryContainer />
              </TabPane>
              <TabPane tab="Compose" key="2">
                <ItemCompose />
              </TabPane>
              <TabPane tab="Finish" key="3">
                 <ItemFinish />
              </TabPane>
            </Tabs>
          </Card>
        </Container>
      </div>
    </>
  )
}

export default AddItem