import React from "react";
import { Form, Row, Col, Button } from 'react-bootstrap';
import { Divider } from 'antd';

const ProfileSettings = () => {
    
    return (
        <>
         <div className={` d-flex  `}>
             <Form className="w-100 p-4">
                 {/* Change Password */}
                    <Form.Group as={Row} controlId="formPassword">
                        <Col sm="9">
                            <Form.Control type="password" disabled placeholder="Change Password" />
                        </Col>
                        <Form.Label column sm="3">
                            <div className="d-flex justify-content-end">
                                <Button variant="primary">Change</Button>
                            </div>
                        </Form.Label>
                    </Form.Group>
                    {/* Change Email */}
                    <Form.Group as={Row} controlId="formEmail">
                        <Col sm="9">
                            <Form.Control type="text" disabled placeholder="Change Email Address" />
                        </Col>
                        <Form.Label column sm="3">
                            <div className="d-flex justify-content-end">
                                <Button variant="primary">Change</Button>
                            </div>
                        </Form.Label>
                    </Form.Group>
                    {/* Change Phone */}
                    <Form.Group as={Row} controlId="formPhone">
                        <Col sm="9">
                            <Form.Control type="number" disabled placeholder="Change Phone Number" />
                        </Col>
                        <Form.Label column sm="3">
                            <div className="d-flex justify-content-end">
                                <Button variant="primary">Change</Button>
                            </div>
                        </Form.Label>
                    </Form.Group>
                    <Divider />
                    <Form.Group as={Row} controlId="formPhone">
                        <Col sm="9">
                            <div className="d-flex flex-column">
                                <h3 className={`  font-16 `}>Deactivate Account</h3>
                                <p className={` font-12 `}>
                                    If you do not plan to use List.am, you can permanently deactivate your account.
                                     Your ads and all other information will be deleted
                                </p>
                            </div>
                        </Col>
                        <Col sm="3">
                            <div className="d-flex justify-content-end">
                                <Button variant="danger">Confirm</Button>
                            </div>
                        </Col>
                    </Form.Group>
             </Form>
         </div>
        </>
    )
}

export default ProfileSettings