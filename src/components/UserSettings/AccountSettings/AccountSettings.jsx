import React from "react";
import { Form, Row, Col, Button } from 'react-bootstrap';

const UserSettings = () => {

    return (
        <>
         <div className={` d-flex justify-content-center `}>
             <Form className="">
                    <Form.Group as={Row} controlId="formEmail">
                        <Form.Label column sm="4">
                        Email
                        </Form.Label>
                        <Col sm="8">
                            <Form.Control type="text" placeholder="Email" />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formLastName">
                        <Form.Label column sm="4">
                        First Name
                        </Form.Label>
                        <Col sm="8">
                            <Form.Control type="text"  />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formFirstName">
                        <Form.Label column sm="4">
                        First Name
                        </Form.Label>
                        <Col sm="8">
                            <Form.Control type="text" />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="formCity">
                        <Form.Label column sm="4">
                        City
                       </Form.Label>
                        <Col sm="8">
                            <Form.Control as="select" >
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </Form.Control>
                        </Col>
                    </Form.Group>
                    <Form.Group>
                        <div className="d-flex justify-content-center">
                            <Button variant="primary">Save</Button>
                        </div>
                    </Form.Group>
             </Form>
         </div>
        </>
    )
}

export default UserSettings