import React from 'react';
import { Container, Row, Col, Nav, Tab } from "react-bootstrap";
import AccountSettings from './AccountSettings/AccountSettings';
import NotificetionsSettings from './NotificetionsSettings/NotificetionsSettings';
import ProfileSettings from './ProfileSettings/ProfileSettings';

const UserSettings = () => {

    return (
        <>
        <Container className="py-5">
                    <Tab.Container className="card" id="left-tabs-example" defaultActiveKey="Auction">
                        <Row>
                            <Col sm={3}>
                                <Nav variant="pills" className="flex-column">
                                    <Nav.Item>
                                        <Nav.Link eventKey="Auction">
                                        Profile
                                        </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="Notificetions">
                                        Notifications
                                        </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey="Profile">
                                        Account
                                        </Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </Col>
                            <Col sm={9}>
                                <Tab.Content>
                                    <Tab.Pane eventKey="Auction">
                                       <AccountSettings />
                                    </Tab.Pane>
                                    <Tab.Pane eventKey="Notificetions">
                                       <NotificetionsSettings />
                                    </Tab.Pane>
                                    <Tab.Pane eventKey="Profile">
                                       <ProfileSettings />
                                    </Tab.Pane>
                                </Tab.Content>
                            </Col>        
                        </Row>
                    </Tab.Container>
        </Container>
        </>
    )
}

export default UserSettings