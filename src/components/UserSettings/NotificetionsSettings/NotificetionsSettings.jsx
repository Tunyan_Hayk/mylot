import React from "react";
import { Form, Button } from 'react-bootstrap';
import { Checkbox } from 'antd';

const NotificetionsSettings = () => {

    function onChange(checkedValues) {
        console.log('checked = ', checkedValues);
      }
    
    return (
        <>
         <div className={` d-flex justify-content-start `}>
             <Form className="">
                    <h3>Send to Email or phone number</h3>
                    <Form.Group>
                        <Checkbox onChange={onChange}>Payment reminder </Checkbox>
                    </Form.Group>
                    <Form.Group>
                        <Checkbox onChange={onChange}>Notification about auction ending (user) </Checkbox>
                    </Form.Group>
                    <Form.Group>
                        <Checkbox onChange={onChange}>Notification to item owner about auction ending </Checkbox>
                    </Form.Group>
                    <Form.Group>
                        <div className="d-flex justify-content-center">
                            <Button variant="primary">Save</Button>
                        </div>
                    </Form.Group>
             </Form>
         </div>
        </>
    )
}

export default NotificetionsSettings