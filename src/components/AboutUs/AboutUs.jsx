import React from 'react';
import classes from './AboutUs.module.css' ;
import AboutTop from '../../assetc/images/AboutUs/About_top.png';
import AboutUnder from '../../assetc/images/AboutUs/About_under.png';

const AboutUs = () => {

    return (
        <div className={classes.about_us}>
            <img src={AboutTop} alt="#" className={classes.AboutTop}/>
            <div className="container">
            
            <h3 className="text-center font-22 font-weight-bold">About Us</h3>

            <div className="text-justify m-5">
                <p>

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Nam in nisi sed augue suscipit convallis eget ac tellus. 
                Vivamus volutpat eget mauris sit amet accumsan. 
                Nam consequat purus eu orci consectetur, ac ultrices turpis venenatis. 
                In hac habitasse platea dictumst. 
                In sagittis enim in eros vulputate eleifend. 
                Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
                In aliquam diam eget dolor vestibulum malesuada. 
                Duis nisl velit, porttitor eget dolor at, gravida pulvinar nisl. 
                Proin lacinia, ante eu suscipit finibus, enim velit facilisis nisl, 
                a volutpat lectus libero et quam.
                </p>
                <p className="mt-3">
                Etiam aliquet risus dignissim felis pellentesque, vitae maximus turpis lacinia. 
                Sed magna neque, pretium quis molestie ac, euismod molestie orci. 
                Pellentesque eleifend porta lacus, quis aliquet magna blandit non. 
                Morbi vel gravida arcu. Suspendisse laoreet tempus massa, vulputate semper est bibendum id. 
                Integer cursus massa a erat scelerisque, 
                id consectetur tortor elementum. Nunc vel metus eget lorem ornare ultricies. 
                Sed vitae turpis ipsum. Sed dictum nec dui nec placerat. Donec gravida volutpat feugiat. 
                Vestibulum in nisi at augue bibendum rhoncus. Duis et auctor nunc. 
                Duis consectetur arcu eu tellus semper viverra. 
                Aliquam ultrices porta quam, a mollis ipsum finibus ac.
                </p>
                <p className="mt-3">
                Pellentesque ac euismod odio. Sed pharetra mauris at eros porta malesuada. 
                Fusce ullamcorper leo in aliquam mollis. Fusce egestas lorem nulla, 
                sed congue velit tincidunt sit amet. Suspendisse id lacus odio. 
                Suspendisse at ligula ac odio feugiat accumsan. Integer nisl leo, 
                euismod quis dignissim malesuada, imperdiet in risus. 
                Proin eu convallis augue. Integer pulvinar lectus vitae leo vehicula, 
                at ullamcorper erat semper. Nam vulputate condimentum ante vehicula tincidunt.
                </p>
                 
            </div>
            </div>
            <div className={` ${classes.AboutImgtwo} `}>
                <img src={AboutUnder} alt="#" className={classes.AboutUnder}/>
            </div>
            
        </div>
    )
}

export default AboutUs