import { ProductsAPI } from "../api/api"

const SET_LIVE_PRODUCTS = 'SET_LIVE_PRODUCTS'

let initialState=  {
    liveproducts : [] 
}

const LiveAuctionReducer = (state = initialState,action) => {
    switch(action.type){
        case SET_LIVE_PRODUCTS : 
            return {
                ...state,
                liveproducts: action.liveproducts
            }
        default:
             return state
    }
}

export const setLiveProducts = (liveproducts) =>{
    return {
        type: SET_LIVE_PRODUCTS,liveproducts
    }
}

export const getLiveProducts = () =>{
   return  (dispatch)=>{
        ProductsAPI.getLiveProducts().then(data => {
              dispatch(setLiveProducts(data.data))
          }) 
    }
} 
   

 
export default LiveAuctionReducer
