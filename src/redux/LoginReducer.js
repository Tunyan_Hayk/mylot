import { LoginAPI } from "../api/api";
import { stopSubmit } from "redux-form";

const SET_LOGIN = 'SET_LOGIN'

let initialState=  {
    login : [
        userId = null,
        username = null ,
        password = null
    ] 
}

const LoginReducer = (state = initialState,action) => {
    switch(action.types){
        case SET_LOGIN : 
            return {
                ...state,
                login: action.userId,
                login: action.username,
                login: action.password
            }
        default:
             return state
    }
}

export const setLogin = ( userId, username, password ) =>{
    return {
        type: SET_LOGIN,login,
        payload : {userId, username, password}
    }
}

export const getLogin = () =>{
   return  (dispatch)=>{
        LoginAPI.getLogin().then(data => {
              dispatch(setLogin(data.data))
          }) 
    }
} 
 
export default LoginReducer
