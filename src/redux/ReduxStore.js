import { combineReducers, createStore, applyMiddleware } from "redux";
import thunkMiddleware from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';
import LiveAuctionReducer from "./LiveAuctionReducer";
import LastChanseAuctionReducer from "./LastChanseAuctionReducer";
import ExpectedAuctionReducer from "./ExpectedAuctionReducer";
import CategoryItemsReducer from "./CategoryItemsReducer";
import RegistrationContainer from "./AuthenticationReducer"


let reducers = combineReducers({
    liveAuction : LiveAuctionReducer,
    lastChanse : LastChanseAuctionReducer,
    expectedAuction : ExpectedAuctionReducer,
    categoryItems : CategoryItemsReducer,
    register : RegistrationContainer,
    form : formReducer,
})

let store = createStore(reducers,applyMiddleware(thunkMiddleware))

export default store