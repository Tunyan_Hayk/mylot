import { ProductsAPI } from "../api/api"

const EXPECTED_PRODUCTS = 'EXPECTED_PRODUCTS'

let initialState=  {
    expectedProducts : [] 
}

const ExpectedAuctionReducer = (state = initialState,action) => {
    switch(action.type){
        case EXPECTED_PRODUCTS : 
            return {
                ...state,
                expectedProducts: action.expectedProducts
            }
        default:
             return state
    }
}

export const setExpectedProducts = (expectedProducts) =>{
    return {
        type: EXPECTED_PRODUCTS,expectedProducts
    }
}

export const getUpcomingProducts = () =>{
   return  (dispatch)=>{
        ProductsAPI.getUpcomingProducts().then(data => {
              dispatch(setExpectedProducts(data.data))
          }) 
    }
} 
   
export default ExpectedAuctionReducer
