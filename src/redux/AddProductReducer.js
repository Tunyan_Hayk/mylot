import { AddProductAPI } from "../api/api"

const SET_ADDPRODUCT = 'SET_ADDPRODUCT'

let initialState=  {
    addProduct : [

    ] 
}

const AddProductReducer = (state = initialState,action) => {
   
    switch(action.type){
        case SET_ADDPRODUCT : 
            return { 
                ...state,
                addProduct: action.addProduct
            }
        default:
             return state
    }
}

export const setAddProduct = (addProduct) =>{
    return {
        type: SET_ADDPRODUCT,addProduct
    }
}

export const postAddProduct = () =>{
   return  (dispatch)=>{
        AddProductAPI.postAddProduct().then(data => {
            
              dispatch(setAddProduct(data.data))
          }) 
    }
} 
 
export default AddProductReducer
