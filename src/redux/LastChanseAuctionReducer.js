import { ProductsAPI } from "../api/api"

const SET_LAST_PRODUCTS = 'SET_LAST_PRODUCTS'

let initialState=  {
    lastChanseProducts : [] 
}

const LastChanseAuctionReducer = (state = initialState,action) => {
    switch(action.type){
        case SET_LAST_PRODUCTS : 
            return {
                ...state,
                lastChanseProducts: action.lastChanseProducts
            }
        default:
             return state
    }
}

export const setLastChanseProducts = (lastChanseProducts) =>{
    return {
        type: SET_LAST_PRODUCTS,lastChanseProducts
    }
}

export const getLastProducts = () =>{
   return  (dispatch)=>{
        ProductsAPI.getLastProducts().then(data => {
              dispatch(setLastChanseProducts(data.data))
          }) 
    }
} 
   

 
export default LastChanseAuctionReducer
