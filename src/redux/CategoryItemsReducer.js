import { CategoryAPI } from "../api/api"

const SET_CATEGORY = 'SET_CATEGORY'

let initialState=  {
    category : [] 
}

const CategoryItemsReducer = (state = initialState,action) => {
   
    switch(action.type){
        case SET_CATEGORY : 
            return { 
                ...state,
                category: action.category
            }
        default:
             return state
    }
}

export const setCategory = (category) =>{
    return {
        type: SET_CATEGORY,category
    }
}

export const getCategoryItems = () =>{
   return  (dispatch)=>{
        CategoryAPI.getCategoryItems().then(data => {
            // console.log("data",data)
              dispatch(setCategory(data.data))
          }) 
    }
} 
   
export default CategoryItemsReducer
