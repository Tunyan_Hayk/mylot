import * as axios from 'axios'


const instance = axios.create({
    baseURL : 'http://admin.mylot.am/api/products',
})
const instanceProject = axios.create({
   baseURL : 'http://admin.mylot.am/api/'
})

export const CategoryAPI= {
   getCategoryItems() {
      return instanceProject.get('/categories')
         .then(response =>{
            return response.data
         })
   }
}

export const RegistreionsAPI = {
   postRegistretion () {
      return instanceProject.post('/auth/register')
      .then(response => {
         return response.date
      })
   }
}

export const LoginAPI = {
   
   postlogin(email, password = false) {
      return instanceProject.post('/auth/login', {
         email,
         password
      })
   },
   logout () {
      return instanceProject.dalete('/auth/login')
   }
}

export const AuthAPI = {
   login( userId, email, password = false) {
      return instanceProject.post('/auth/login', {
         userId,
         email,
         password
      })   
   },
   logout (){
      return instanceProject.dalete('/auth/login')
   }
}

export const AddProductAPI = {
   postAddProduct() {
      return instanceProject.post(' /product ')
      .then(response => {
         return response.date
      })
   }
}

export const ProductsAPI ={
    getUpcomingProducts (){
        return instance.get('/upcoming_auctions?type=paginate')
         .then(response =>{
            return response.data
         } )
     },
     getLiveProducts (){
        return instance.get('/live_auctions?type=paginate')
         .then(response =>{
            return response.data
         } )
     },
     getLastProducts (){
        return instance.get('/last_chanse?type=paginate')
         .then(response =>{
            return response.data
         } )
     },
}